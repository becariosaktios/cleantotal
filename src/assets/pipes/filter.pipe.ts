import { Pipe, PipeTransform } from '@angular/core';

@Pipe ({ name: 'appFilter', pure: false})
export class FilterPipe implements PipeTransform {
      /*
    Transform
   
   @param {any[]} items
   @param {string} searchText
   @returns {any[]}
   */
  transform(items: any[], paginador:{
    searchText: string,
    page: number,
    pageSize: number
    }): any[] {
    if (!items) {
      return [];
    }
    if (!paginador) {
      return items;
    }
    const searchText = paginador.searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.name.toLocaleLowerCase().includes(searchText)
      || it.surname.toLocaleLowerCase().includes(searchText)
      || it.surname2.toLocaleLowerCase().includes(searchText);
    }).slice(paginador.pageSize * paginador.page, paginador.pageSize * (paginador.page + 1));
  }
}