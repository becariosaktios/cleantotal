import { Pipe, PipeTransform } from '@angular/core';
import * as datasource from 'src/assets/json/datasource.json';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch(value){
      case 1 : return (datasource.data.country[0].description);
      case 2 : return (datasource.data.country[1].description);
      case 3 : return (datasource.data.country[2].description);
    }
    
    
    return null;
  }

}
