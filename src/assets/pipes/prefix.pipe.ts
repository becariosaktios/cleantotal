import { Pipe, PipeTransform } from '@angular/core';
import * as datasource from 'src/assets/json/datasource.json';

@Pipe({
  name: 'prefix'
})
export class PrefixPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch(value){
      case 1 : return (datasource.data.country[0].prefix);
      case 2 : return (datasource.data.country[1].prefix);
      case 3 : return (datasource.data.country[2].prefix);
    }
    
    
    return null;
  }

}
