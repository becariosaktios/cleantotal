import { Component, OnInit } from '@angular/core';
import * as datasource from 'src/assets/json/datasource.json';
import * as info from 'src/assets/json/info-population.json';
@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  title = 'cleanTotal';
  color = 'red';
  Datos: any;
  Info: any;
  paginador: {
    searchText: string,
    page: number,
    pageSize: number,
    list: any[]
  } = {
    searchText: '',
    page: 0,
    pageSize:10,
    list: []
  };


  constructor(){
    
  }

  ngOnInit(){
    this.Datos = this.getJsonContent1();

   
    this.Info = this.getJsonContent2();

  }
  getJsonContent1(){
    return (datasource.data);
  }
  getJsonContent2(){
    return (info.population);
  }

}
