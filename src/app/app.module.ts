import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FilterPipe } from 'src/assets/pipes/filter.pipe';
import { CountryPipe } from 'src/assets/pipes/country.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrefixPipe } from 'src/assets/pipes/prefix.pipe';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ListaComponent } from './lista/lista.component';
import { FormularioComponent } from './formulario/formulario.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    CountryPipe,
    PrefixPipe,
    ListaComponent,
    FormularioComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatPaginatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
